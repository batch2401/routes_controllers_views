<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Post;

class PostController extends Controller
{
    //

    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        //check if the user is logged in
        if(Auth::user()){
            //create a new Post object from the post model
            $post = new Post;

            //define the properties of the $post object using the received form data
            $post->title = $request->input('title');
            $post->content = $request->input('content');
            //get the id of the authenticated user and set it as the user_id foreign key
            $post->user_id = Auth::user()->id;
            //save the $post object to the database
            $post->save();

            return redirect('/posts');
        }else{
            //redirect the user to the login page if not logged in
            return redirect('/login');
        }
    }

    public function index()
    {
        //get all posts from the database
        $posts = Post::all();
        return view('posts.index')->with('posts', $posts);
    }
}
